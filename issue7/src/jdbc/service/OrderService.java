package jdbc.service;

public interface OrderService {
	void userInputInsertRecord();
	void userChoice();
	void deleteRecord();
	void updateRecord();
}
