package jdbc.service;

import java.util.Scanner;

import jdbc.dao.OrderDAO;
import jdbc.dao.OrderDAOImpl;
import jdbc.pojo.Order;

public class OrderServiceImpl implements OrderService {

	OrderDAO refUserDAO;
	Scanner scannerRef;
	Order refUser;

	@Override
	public void userInputInsertRecord() {
		scannerRef = new Scanner(System.in);

		System.out.println("Enter Order ID : ");
		int orderID = scannerRef.nextInt();

		System.out.println("Enter Order Code : ");
		String orderCode = scannerRef.next();
		
		System.out.println("Enter Price : ");
		double price = scannerRef.nextDouble();
		
		refUser = new Order();
		refUser.setOrderID(orderID);
		refUser.setOrderCode(orderCode);
		refUser.setPrice(price);

		refUserDAO = new OrderDAOImpl();
		refUserDAO.insertRecord(refUser);
	}



	@Override
	public void userChoice() {
		System.out.println("Enter Choice");
		System.out.println("1: Insert New Order Record");
		System.out.println("2: Delete Record");
		System.out.println("3: Update Record");

		scannerRef = new Scanner(System.in);
		int choice = scannerRef.nextInt();
		switch (choice) {
		case 1:
			userInputInsertRecord();
			break;
//		case 2:
//			userLogin();
//			break;
		case 2:
			deleteRecord();
			break;
		case 3:
			updateRecord();
			break;
		
		default:
			System.out.println("Option not found..");
			break;
		} // end of userChoice

	}

	@Override
	public void deleteRecord() {
		scannerRef = new Scanner(System.in);

		System.out.println("Enter Order ID to delete : ");
		int orderID = scannerRef.nextInt();

//		System.out.println("Enter User Password : ");
//		String userPassword = scannerRef.next();

		refUser = new Order();
		refUser.setOrderID(orderID);
//		refUser.setOrderCode(orderCode);
//		refUser.setPrice(price);
//	
		refUserDAO = new OrderDAOImpl();
		refUserDAO.deleteRecord(refUser);
	}

	@Override
	public void updateRecord() {
		scannerRef = new Scanner(System.in);

		System.out.println("Enter Order ID to update : ");
		int orderID = scannerRef.nextInt();

		//System.out.println("Enter New Password : ");
		//String newUserPassword = scannerRef.next();
		
		System.out.println("Enter New Order Code : ");
		String orderCode = scannerRef.next();
		
//		System.out.println("Enter New Price : ");
//		Double price = scannerRef.nextDouble();
		
		
		refUser = new Order();
		refUser.setOrderID(orderID);
		refUser.setOrderCode(orderCode);
		//refUser.setUserPassword(newUserPassword);

		refUserDAO = new OrderDAOImpl();
		refUserDAO.updateRecord(refUser);

	}



}
