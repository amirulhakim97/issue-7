package jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import jdbc.pojo.Order;
import utility.DBUtil;

public class OrderDAOImpl implements OrderDAO {

	Connection refConnection = null;
	PreparedStatement refPreparedStatement = null;

	@Override
	public void insertRecord(Order refUser) {
		try {
			refConnection = DBUtil.getConnection();

			// mysql insert query
			String sqlQuery = "insert into orders(orderID,orderCode,price) values(?,?,?)";

			// create the mysql insert preparedstatement
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1, refUser.getOrderID());
			refPreparedStatement.setString(2, refUser.getOrderCode());
			refPreparedStatement.setDouble(3, refUser.getPrice());

			// Approach 1
			refPreparedStatement.execute(); // works fine
			System.out.println("Record has been inserted successfully.");

			// Approach 2
//			int record = refPreparedStatement.executeUpdate();
//			if(record>0) System.out.println("new record has been successfully inserted");

		} catch (Exception e) {
			System.out.println("Exception Handled while insert record.");
		}

		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				System.out.println("Closing Connection..");
			}
		}
	}

	@Override
	public void deleteRecord(Order refUser) {
		try {
			refConnection = DBUtil.getConnection();

			// mysql insert query
			String sqlQuery = "delete from orders where orderID=?";

			// create the mysql insert preparedstatement refPreparedStatement =
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setInt(1, refUser.getOrderID());

			// Approach 1
			refPreparedStatement.execute(); // works fine
			System.out.println("Record has been deleted successfully.");

			// Approach 2 // int record = refPreparedStatement.executeUpdate(); //
//			if (record > 0)
//				System.out.println("new record has been successfully deleted");

		} catch (Exception e) {
			System.out.println("Exception Handled while deleting record.");
		}

		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				System.out.println("Closing Connection..");
			}
		}
	}

	@Override
	public void updateRecord(Order refUser) {
		try {
			refConnection = DBUtil.getConnection();

			// mysql insert query
			String sqlQuery = "update orders set orderCode=? where orderID=?";
			
			// create the mysql insert preparedstatement refPreparedStatement =
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			refPreparedStatement.setString(1, refUser.getOrderCode());
			refPreparedStatement.setInt(2, refUser.getOrderID());
			

			// Approach 1 
			refPreparedStatement.execute(); // works fine
			System.out.println("Record has been updated successfully.");

			// Approach 2 // int record = refPreparedStatement.executeUpdate(); //
			//if (record > 0)
				//System.out.println("new record has been successfully deleted");

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception Handled while updating record.");
		}

		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				System.out.println("Closing Connection..");
			}
		}
	}

}
