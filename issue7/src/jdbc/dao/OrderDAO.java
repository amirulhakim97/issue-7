package jdbc.dao;

import jdbc.pojo.Order;

public interface OrderDAO {
	
	void insertRecord(Order refOrder);
	void deleteRecord(Order refOrder);
	void updateRecord(Order refOrder);

	
}